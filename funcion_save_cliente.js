function saveClient(req,res){
        //Obtenemos la variable del formulario
        const cedula = req.body.cedula;
		const nombre = req.body.nombre;
        const apellido =req.body.apellido;
		const direccion = req.body.direccion;
        const telefono = req.body.telefono;
        const email = req.body.email;
        
        //Validamos que estén los datos
        if (!cedula && !nombre && !apellido && !direccion && !telefono &&!email) {

            return res.render('insertarclientes', {
                message: 'Los datos se han ingresando'
            });
        }
        conexion.query('SELECT * FROM cliente WHERE Cedula = ? AND Apellido = ? AND Nombre = ?  AND Telefono = ? AND Direccion=? AND Email=? ', [nombre,apellido, cedula, telefono, direccion, email], (error, result) => {

            if (result == 0) {
                conexion.query('INSERT INTO cliente SET ?', { Nombre:nombre, Apellido:apellido, Cedula:cedula, Telefono:telefono, Direccion:direccion, Email:email }, (error, results) => {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect('/Listcliente');
                    }
                });

            } else {
                console.log('cliente duplicado')
                res.render('clientesInsertados', {
                    message: 'cliente ya se encuentra en la BD'
                });
            }
        })

};

